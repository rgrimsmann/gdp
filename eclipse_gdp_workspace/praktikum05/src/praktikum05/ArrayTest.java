package praktikum05;

import java.util.Arrays;

public class ArrayTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double[] test = { 10, 11, 12, 13, 14 };
		int[] testint = { 10, 11, 12, 13, 14 };
		long[] testl = { 1, 12, 13, 16, 17 };
		int[] a = { 2, 3, 5, 4, 0, 1 };
		char[] testchar = { 'H', 'a', 'l', 'l', 'o', ' ', 'W', 'o', 'r', 'l', 'd' };
		double[] test2 = Arrays.copyOf(test, test.length);
		System.out.println(Arrays.toString(test2));
		System.out.println(getMaximum(test));
		System.out.println(getMaximum(new double[]{}));
		System.out.println(countOdd(testint));
		System.out.println(Arrays.toString(revert(testchar)));
		System.out.println(isSorted(testl));
		System.out.println(isHopPossible(a));
	}

	/**
	 * getMaximum(double[] a) return -1 when a[] == null
	 * 
	 * @param a
	 *            double[]
	 * @return maximum or NaN
	 */
	public static double getMaximum(double[] a) {
		if (a != null && a.length != 0) {
			double max = a[0];
			for (int i = 0; i < a.length; i++) {
				if (max < a[i]) {
					max = a[i];
				}
			}
			return max;
		}
		return Double.NaN;
	}

	/**
	 * countOdd(int[] a) return -1 when a == null
	 * 
	 * @param a
	 *            int[]
	 * @return the amount of odd numbers in a[]
	 */
	public static int countOdd(int[] a) {
		if (a != null) {
			int cnt = 0;
			for (int i = 0; i < a.length; i++) {
				if (a[i] % 2 != 0)
					cnt++;
			}
			return cnt;
		}
		return -1;
	}

	/**
	 * revert(char[] a) return insertnullerror when a == null
	 * 
	 * @param a
	 * @return insertnullerror or reverted char array
	 */
	public static char[] revert(char[] a) {
		if (a != null) {
			char[] rvrt = new char[a.length];
			for (int i = 0; i < a.length; i++) {
				int cnt = (a.length - 1) - i;
				rvrt[i] = a[cnt];
				rvrt[cnt] = a[i];
			}
			return rvrt;
		}
		return "insertnullerror".toCharArray();
	}
	public static int[] revert(int[] a) {
		if (a != null) {
			int[] rvrt = new int[a.length];
			for (int i = 0; i < a.length; i++) {
				int cnt = (a.length - 1) - i;
				rvrt[i] = a[cnt];
				rvrt[cnt] = a[i];
			}
			return rvrt;
		}
		return null;
	}
	
	public static double[] revert(double[] a) {
		if (a != null) {
			double[] rvrt = new double[a.length];
			for (int i = 0; i < a.length; i++) {
				int cnt = (a.length - 1) - i;
				rvrt[i] = a[cnt];
				rvrt[cnt] = a[i];
			}
			return rvrt;
		}
		return null;
	}


	/**
	 * isSorted(long[] a) return false if a == null
	 * 
	 * @param a
	 * @return true or false
	 */
	public static boolean isSorted(long[] a) {
		if (a == null)
			return false;
		boolean rt = true;
		for (int i = 1; i < a.length; i++) {
			if (a[i - 1] > a[i])
				return false;
		}
		return rt;
	}

	/**
	 * isHopPossibl return true if all indexes are be hopped
	 * 
	 * @return boolean
	 */
	public static boolean isHopPossible(int[] a) {
		int cnt = 0;
		while (cnt >= 0 && cnt < a.length) {
			int h = cnt;
			cnt = a[cnt];
			a[h] = -1;
		}
		for (int i = 0; i < a.length; i++) {
			if (a[i] != -1)
				return false;
		}
		return true;
	}
}
