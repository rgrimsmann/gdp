package praktikum05;

public class Matrix {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int m = 3;
		int n = 3;
		double[][] a = new double[n][m]; // 1 x m matrix7 anzahl spalten 3
											// anzahl zeilen m
		a = new double[][] { { 1, 2, 3 }, { 2, 3, 4 }, { 3, 4, 5 } };
		double[][] b = new double[][] {};
		b = new double[][] { { 2, 1 }, { 2, 1 }, { 2, 1 } };
		// b = new double[][] { { 2, 1 }, { 2, 1 } };

		System.out.println(isMatrix(a, 3, 3));
		System.out.println(isMatrix(new double[][] { null }, 1, 1));
		System.out.println(isMatrix(new double[][] { { Double.NaN, Double.NEGATIVE_INFINITY } }, 1, 2));
		System.out.println(a.length + " || " + a[0].length);
		// printMatrix(a);
		// printMatrix(b);
		printMatrix(multMatrix(a, b));
	}

	public static boolean isMatrix(double[][] a, int l, int m) {
		if (a == null || a.length == 0 || a.length != l)
			return false;

		if (l <= 0 || m <= 0)
			return false;
		for (int i = 0; i < a.length; i++) {
			if ( a[i].length != m || a[i] == null){
//				Double.isFinite(d)
				return false;
			}
		}

		return true;
	}

	// double [sp][ze]
	public static double[][] multMatrix(double[][] a, double[][] b) {
		double[][] ergebnis = null;
		if (a != null && b != null) {
			if ((a[0].length == b.length)) {
				ergebnis = new double[a.length][b[0].length];
				int a_zeile = 0;
				for (int d = 0; d < ergebnis.length; d++) {
					double[] zeile = new double[b[0].length];
					int b_spalte = 0;
					for (int i = 0; i < zeile.length; i++) { // Zeile von a
						int b_zeile = 0;
						double erg = 0;
						for (int j = 0; j < a[i].length; j++) { // spalte x
							erg += a[a_zeile][j] * b[b_zeile][b_spalte];
							b_zeile++;
						}
						zeile[i] = erg;
						b_spalte++;
					}
					a_zeile++;
					ergebnis[d] = zeile;
				}

			}

		}
		return ergebnis;
	}

	public static double[][] multMatrixa(double[][] a, double[][] b) {
		if (a == null || b == null)
			return null;

		if ((a[0].length == b.length)) {
			double[][] ergebnis = new double[a.length][b[0].length];
			for (int i = 0; i < ergebnis.length; i++) {
				for (int j = 0; j < ergebnis[0].length; j++) { // Zeile von a
					for (int k = 0; k < b.length; k++) { // spalte x
						ergebnis[i][j] += a[i][k] * b[k][j];
					}
				}
			}
			return ergebnis;
		}

		return null;
	}

	public static void printMatrix(double[][] a) { // zwei dim Matrix
		String line = "Matrix null!";
		if (a != null) {
			line = new String();
			for (int i = 0; i < a.length; i++) {
				for (int j = 0; j < a[i].length; j++) {
					line += " " + a[i][j] + " ";
				}
				line += "\n";
			}
		}
		System.out.println(line);
	}
}
