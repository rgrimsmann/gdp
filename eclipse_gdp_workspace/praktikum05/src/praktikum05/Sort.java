package praktikum05;

import java.util.Arrays;
import java.util.Random;

import edu.unibw.etti.ArrayGenerator;
import edu.unibw.etti.ArrayVisualizer;

public class Sort {
	static int ccnt;
	static int vcnt;

	public static void main(String[] args) {
		double[] a = arrayGen(80, 1000);
		// int[] aa = ArrayGenerator.generateRandomIntArrayWithoutPutBack(100,
		// 1000);
		double[] b = ArrayTest.revert(a);
		double[] c = { -2, 1, -3, 4 };

		ArrayVisualizer.add(a);
		ArrayVisualizer.setZoom(100);
		ArrayVisualizer.switchToTwoDimensionalExtendedVisualization();
		ArrayVisualizer.update();
		// quicksort(0, b.length -1, b);
		selectionSort(c);
		
		System.out.println(Arrays.toString(c));
		System.out.println( " Vertauschungen: " + ccnt + " \nVergleiche:  " + vcnt);
		test();
	}

	public static void test() {
		selectionSort(new double[] {});
		selectionSort(new double[] { 1.0 });
		selectionSort(new double[] { 3.0, 2.5, 1.0, 2.5 });

	}

	/*
	 * public static void quicksort(int l, int r, double[] intArr) { int q; if
	 * (l < r) { q = partition(l, r, intArr); quicksort(l, q, intArr);
	 * quicksort(q + 1, r, intArr); } }
	 * 
	 * public static int partition(int l, int r, double[] intArr) { int i, j;
	 * double x = intArr[(l + r) / 2]; i = l - 1; j = r + 1;
	 * 
	 * do { i++; } while (intArr[i] < x);
	 * 
	 * do { j--; } while (intArr[j] > x);
	 * 
	 * if (i < j) { double k = intArr[i]; intArr[i] = intArr[j]; intArr[j] = k;
	 * } else { return j; } return -1; }
	 */
	public static void selectionSort(double[] a) {
		ccnt = 0;
		vcnt = 0;
		if (a != null) {
			int min = 0;
			for (int l = 0; l < a.length; l++) {
				min = l;
				for (int i = l + 1; i < a.length; i++) {
					vcnt++;
					if (a[i] < a[l]) {
						min = i;
					}
				}
				if (min != l) {
					double h = a[min];
					a[min] = a[l];
					a[l] = h;
					ccnt++;
				}
				// l++;
				ArrayVisualizer.update();
			}
		}

	}

	public static void selectionSort(int[] a) {
		if (a != null) {
			int l = 0;
			int min = 0;
			while (l < a.length) {
				min = l;
				for (int i = l + 1; i < a.length; i++) {
					if (a[i] < a[min])
						min = i;
				}
				int h = a[min];
				a[min] = a[l];
				a[l] = h;
				l++;
				ArrayVisualizer.update();
			}
		}
	}

	public static double[] arrayGen(int high, int l) {
		Random r = new Random();
		double[] a = new double[l];
		for (int i = 0; i < l; i++) {
			double rand = Math.abs(r.nextInt() % high);
			a[i] = rand;
		}
		return a;
	}

}
