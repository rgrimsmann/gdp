
public class SeqDLLOperation {
	public static String getString(SeqByDLL seq, boolean inverted) {
		String ret = "";
		if (seq != null) {
			if (inverted) {
				if (seq.tail != null) {
					ret += "(";
					DLLInt s = seq.tail;
					while (s != null) {
						ret += s.element + ((s.prev != null) ? ", " : "");
						s = s.prev;
					}
					ret += ") <-";
				} else {
					ret = "Liste Tail Null";
				}
			} else {
				if (seq.head != null) {
					ret += "-> (";
					DLLInt s = seq.head;
					while (s != null) {
						ret += s.element + ((s.next != null) ? ", " : "");
						s = s.next;
					}
					ret += ")";
				}
			}
		} else {
			ret = "Liste Null";
		}
		return ret;
	}

	public static SeqByDLL insert(int val, SeqByDLL seq) {
		SeqByDLL head = seq;
		if (seq == null) {
			DLLInt n = new DLLInt(null, null, val);
			return new SeqByDLL(n, n);
		} else if (seq.head == null && seq.tail == null) {
			DLLInt n = new DLLInt(null, null, val);
			seq.head = n;
			seq.tail = n;
		} else if (seq.head.element >= val) {
			DLLInt n = new DLLInt(null, seq.head, val);
			seq.head.prev = n;
			seq.head = n;
		} else if (seq.tail.element <= val) {
			DLLInt n = new DLLInt(seq.tail, null, val);
			seq.tail.next = n;
			seq.tail = n;
		} else {
			DLLInt seqhead = seq.head;
			while (seqhead != null) {
				if (val <= seqhead.element) {
					DLLInt n = new DLLInt(seqhead.prev, seqhead, val);
					// n.prev.next = n;
					// muss gelten n.next.prev = n;
					seqhead.prev.next = n;
					seqhead.prev = n;
					return seq;
				} else {
					//
				}
				seqhead = seqhead.next;
			}
		}
		return head;
	}

	public static SeqByDLL revert(SeqByDLL seq) {
		DLLInt h = seq.head; // 1 -> 2 -> 3 next
		DLLInt t = seq.tail; // 1 <- 2 <- 3 prev
		while (h != null) {
			h.prev = t;
			h = t.prev;
		}
		return seq;
	}

	public static void sort(DLLInt seq) {
		while (seq != null && seq.next != null) {
			if (seq.element > seq.next.element) {
				int h = seq.element;
				seq.element = seq.next.element;
				seq.next.element = h;
			}
			seq = seq.next;
		}
	}
}