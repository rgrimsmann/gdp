
public class DLLInt {
	public DLLInt prev;
	public DLLInt next;
	public int element;

	public DLLInt() {
		// TODO Auto-generated constructor stub
		prev = null;
		next = null;
		element = 0;
	}

	public DLLInt(DLLInt p, DLLInt n, int e) {
		prev = p;
		next = n;
		element = e;
	}
}
