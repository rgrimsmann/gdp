
public class SLLInt {
	public int element;
	public SLLInt next;

	public SLLInt(int e, SLLInt n) {
		element = e;
		next = n;
	}

	public SLLInt(int e) {
		element = e;
		next = null;
	}
}
