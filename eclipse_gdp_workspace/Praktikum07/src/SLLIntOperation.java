
public class SLLIntOperation {

	public static SLLInt selectOdd(SLLInt seq) {
		if (seq == null) {
			return null;
		}
		if (seq.element % 2 == 0) {
			return selectOdd(seq.next);
		} else {
			return new SLLInt(seq.element, selectOdd(seq.next));
		}
	}

	public static SLLInt selectEven(SLLInt seq) {
		if (seq == null) {
			return null;
		}
		if (seq.element % 2 != 0) {
			return selectOdd(seq.next);
		} else {
			return new SLLInt(seq.element, selectOdd(seq.next));
		}
	}

	public static SLLInt selectEvenNR(SLLInt seq) {
		SLLInt head = null;
		SLLInt tail = null;
		while (seq != null) {
			if (tail == null) {
				if (seq.element % 2 == 0) {
					head = new SLLInt(seq.element);
					tail = head;
				}
			} else {
				if (seq.element % 2 == 0) {
					tail.next = new SLLInt(seq.element);
					tail = tail.next;
				}
			}
			seq = seq.next;
		}
		return head;
	}

	public static String getString(SLLInt seq) {
		if (seq == null) {
			return ".";
		} else if (seq.next == null) {
			return "" + seq.element + ".";
		} else {
			return seq.element + ":" + getString(seq.next);
		}
	}

	public static int getLength(SLLInt seq) {
		int cnt = 0;
		while (seq != null) {
			cnt++;
			seq = seq.next;
		}
		return cnt;
	}

	public static boolean isSorted(SLLInt seq) {
		if (getLength(seq) <= 1) {
			return true;
		} else {
			SLLInt sa = seq;
			while (sa.next != null) {
				if (sa.element > sa.next.element) {
					return false;
				}
				sa = sa.next;
			}
		}
		return true;
	}

	public static SLLInt insert(int value, SLLInt seq) {
		SLLInt head = seq;
		if (seq == null) {
			seq = new SLLInt(value);
		} else if (seq.element >= value) {
			return new SLLInt(value, seq);
		} else {
			while (seq != null) {
				if(seq.element < value && seq.next == null) { // ende
					SLLInt s = new SLLInt(value);
					seq.next = s;
				}else if(seq.element < value && seq.next.element >= value && seq.next != null) {
					SLLInt s = new SLLInt(value, seq.next); 
					seq.next = s;
				}
				seq = seq.next;
			}
		}
		return head;
	}

	public static SLLInt insertAndCopy(int value, SLLInt seq) {
		if (seq == null)
			return new SLLInt(value);
		if (value <= seq.element) {
			return new SLLInt(value, insertAndCopy(seq.element, seq.next));
		} else {
			return new SLLInt(seq.element, insertAndCopy(value, seq.next));
		}
	}

	public static SLLInt addElementAndCopy(int value, SLLInt seq) {
		if (seq == null) {
			return new SLLInt(value);
		} else {
			SLLInt cpy = copy(seq);
			last(cpy).next = new SLLInt(value);
			return cpy;
		}
	}

	public static SLLInt copyAndRemove(int value, SLLInt seq) {
		SLLInt head = null;
		SLLInt tail = null;
		while (seq != null) {
			if (tail == null) {
				if (seq.element == value) {
					head = new SLLInt(seq.element);
					tail = head;
				}
			} else {
				if (seq.element == value) {
					tail.next = new SLLInt(seq.element);
					tail = tail.next;
				}
			}
			seq = seq.next;
		}
		return head;
	}

	public static SLLInt remove(int value, SLLInt seq) {
		if (seq == null) {
			return null;
		} else if (seq.element == value) {
			return remove(value, seq.next);
		} else {
			seq.next = remove(value, seq.next);
			return seq;
		}
	}

	public static SLLInt copy(SLLInt seq) {
		SLLInt cpyA = null;
		if (seq == null)
			return seq;
		else {
			SLLInt a = seq.next; // Sexy Back :D
			cpyA = new SLLInt(seq.element);
			while (a != null) {
				SLLInt n = new SLLInt(a.element);
				SLLInt ll = last(cpyA); // aktuelles ende finden
				ll.next = n;
				a = a.next;
			}
			return cpyA;
		}
	}

	public static boolean hasElement(SLLInt seq, int value) {
		SLLInt ne = seq;
		while (ne != null) {
			if (ne.element == value)
				return true;
			ne = ne.next;
		}
		return false;
	}

	public static void sort(SLLInt seq) {
		if (seq != null) {
			while (!isSorted(seq)) {
				if (seq.element > seq.next.element) {
					int h = seq.element;
					seq.element = seq.next.element;
					seq.next.element = h;
				}
				seq = seq.next;
			}
		}
	}

	public static SLLInt last(SLLInt seq) {
		if (seq != null && seq.next == null) {
			return seq;
		} else {
			return last(seq.next);
		}
	}
}
