import java.awt.Color;

import edu.unibw.etti.SimpleGraphicPanel;



public class Reihe {

	public static void main(String[] args) {
		System.out.println(geometrischeReihe((double) (1.0 / 3.0)));
//		maleDreick(100, 100, 150, 150);
//		maleDreick(100, 100, 150, 75);
//		maleDreick(100, 100, 150, 37.5);
		maleDreick(100, 100, 500, 18.75);
//		maleDreick(100, 100, 150, 9.375);
		System.out.println(wallissischesProdukt(100000));
	}
	
	public static double geometrischeReihe() {
		double q = 0.5;
		double summe = 1;
		double qn = 1;
		int pow = 1;
		boolean run = true;
		while (run) {
			summe += Math.pow(q, (double) pow);
			pow++;
			if (summe == qn) {
				run = false;
			} else {
				qn = summe;
			}
		}
		return summe;
	}
	//Kein Kommentar! 
	public static double geometrischeReihe(double q) {
		double summe = 1;
		double qn = 1;
		int pow = 1;
//		boolean run = true;
		do {
			qn = summe;
			summe += Math.pow(q, (double) pow);
			pow++;
			
		} while (summe != qn);
		return summe;
	}

	public static double wallissischesProdukt(double abbruch) {
		double i = 2.0;
		double j = 1.0;
		double half_pi = 1.0f;
		while (i < abbruch) {
			half_pi *= i / j;
			double h = i;
			i = j + 1;
			j = h + 1;
		}
		return half_pi * 2;
	}

	public static void maleDreick(double ax, double ay, double s, double t) {
		double bx = berechneBx(ax, s);
		double by = berechneBy(ay);
		double cx = berechneCx(ax, bx);
		double cy = berechneCy(ax, ay, bx);
		double px = berechnePx(ax, cx),
			   py = berechnePy(ay, cy);
		double qx = berechneQx(bx, cx),
			   qy = berechneQy(ay, by);
		double rx = berechneRx(ax, bx), 
			   ry = berechneRy(ay, by);
		if(s <= t) {
			SimpleGraphicPanel.drawTriangle(Color.BLACK, true, ax, ay, bx, by, cx, cy);
		} else {
			s = s / 2;
			maleDreick(ax, ay, s, t);
			maleDreick(px, py, s, t);
			maleDreick(rx, ry, s, t);
		}
	}

	public static double berechneBx(double ax, double s) {
		return ax + s;
	}

	public static double berechneCx(double ax, double bx) {
		return (ax + bx) / 2.0;
	}

	public static double berechneCy(double ax, double ay, double bx) {
		return ay + Math.sqrt(3.0) * (bx - ax) / 2.0;
	}

	public static double berechnePx(double ax, double cx) {
		return (ax + cx) / 2.0;
	}

	public static double berechnePy(double ay, double cy) {
		return (ay + cy) / 2.0;
	}

	public static double berechneQx(double bx, double cx) {
		return (bx + cx) / 2.0;
	}
	
	public static double berechneQy(double ay, double by) {
		return (ay + by) / 2.0;
	}
	public static double berechneRx(double ax, double bx) {
		return (ax + bx) / 2.0;
	} 
	public static double berechneRy(double ay, double by) {
		return (ay + by) / 2.0;
		
	}
	public static double berechneBy(double ay) {
		return ay;
	}
}