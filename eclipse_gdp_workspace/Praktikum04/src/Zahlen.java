
public class Zahlen {

	public static void main(String[] args) {
		// System.out.println(fakultaet(12));
		System.out.println(fakultaet(12));
		System.out.println(fakultaet((long)20));
		System.err.println(fakIntTest());
		System.err.println(fakLongTest());
		System.out.println(quersumme(88));
		System.out.println(isHarshadZahl(19));
		printHarshadZahl(100);
		System.out.println(teilersumme(12));
		printVollkommeneZahl(10000);
	}
	
	public static int fakIntTest() {
		int x = 0;
		int y = 0;
		for (int i = 1; i < 15; i++) {
			x = fakultaet(i);
			y = fakultaet(i + 1);
			if (x > y) {
				return i -1;
			}
		}
		return -1;
	}
	
	public static int fakLongTest() {
		long x = 0;
		long y = 0;
		for (int i = 1; i < 22; i++) {
			x = fakultaet((long)i);
			y = fakultaet((long)(i + 1));
			if (x > y) {
				return i;
			}
		}
		return -1;
	}
	
	public static int fakultaet(int n) { //Grenze im Debugger ermittelt
		int fak = 1;
		if (n >= 0) {
			for (int i = 2; i <= n; i++) {
				if(fak > Integer.MAX_VALUE / i) return -1;
				else fak = fak * i;
			}
		} else {
			fak = -1;
		}
		return fak;
	}

	public static long fakultaet(long n) {
		long fak = 1;
		if (n >= 0) {
			for (long i = 2; i <= n; i++) {
				fak = fak * i;
			}
		} else {
			fak = -1;
		}
		return fak;
	}

	public static long quersumme(long n) {
		long summe = 0;
		while (n != 0) {
			summe += (n % 10);
			n = n / 10;
		}
		return summe;
	}

	public static boolean isHarshadZahl(long n) {
		if (n == 0) return false;
		return (n % quersumme(n) == 0);
	}

	public static void printHarshadZahl(long n) {
		for (long i = 0; i < n; i++) {
			if(isHarshadZahl(i)) {
				System.out.println(i + " mit Quersumme " + quersumme(i) + " == 0 teilbar!");
			}
		}
	}
	
	public static long teilersumme(long n) {
		long summe = 0;
		for(long i = 1; i < n; i++) {
			summe += ((n % i) == 0) ? i :0; 
		}
		return summe;
	}
	
	public static void printVollkommeneZahl(long n) {
		for (long i = 0; i < n; i++) {
			if(teilersumme(i) == i) {
				System.out.println(i + " ist eine vollkommene Zahl");
			}
		}
	}
}
