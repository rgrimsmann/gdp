import edu.unibw.etti.graphics.*;

import java.awt.Color;
import java.awt.Graphics;

import Model.Dreieck;
import Model.Kreis;
import Model.Position;
import Model.Viereck;

public class Main {
	public static void main(String[] args) {
		GraphicsPanel g = new GraphicsPanel("Praktikum", Position.X_MAX, Position.Y_MAX);
		Graphics gg = g.createGraphics();
		gg.setColor(Color.BLUE);
		gg.fillRect(0, 0, Position.X_MAX, Position.Y_MAX);
		Kreis k = new Kreis(Color.BLACK, null, new Position(100, 100), 1);
		k.anzeigen(gg);
		Kreis k2 = new Kreis(null, Color.yellow, new Position(100, 100), 25);
		k2.anzeigen(gg);
//		Viereck v = new Viereck(Color.green, Color.magenta, new Position(200, 200), 100, 50);
//		v.anzeigen(gg);
//		Dreieck d = new Dreieck(Color.darkGray, Color.PINK, new Position(150, 200), 80, 80);
//		d.anzeigen(gg);
//		Dreieck dd = new Dreieck(null, Color.BLACK, new Position(150, 200), 80, 80);
//		dd.anzeigen(gg);
		// gg.fillRect(0, 0, Position.X_MAX, Position.Y_MAX);
		// gg.setColor(Color.red);
		// gg.fillOval(100, 100, 50, 50);
		//
		// gg.clearRect(0, 0, 600, 400);
		// g.showGraphics();
		/*
		 * for (int i = 0; i < 100; i++) { gg.setColor(Color.WHITE);
		 * gg.fillRect(0, 0, 300, 200); gg.setColor(Color.BLACK);
		 * gg.drawOval(50+i, 50, 30, 20); gg.fillOval(100, 50+i, 30, 20);
		 * GraphicsPanel.sleep(5); g.showGraphics(); }
		 */
		g.showGraphics();

	}
}
