package Model;

public abstract class Medium {
	private String title;
	public Medium(String title) {
		this.title = title;
	}
	public abstract String ausgeben();
	
}
