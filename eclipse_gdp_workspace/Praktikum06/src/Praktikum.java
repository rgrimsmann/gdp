
public class Praktikum {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] a = { 1, 4, 6, 8, 11, 15, 61 }; // l - 1 = 6
		System.out.println(getIndex(a, 61));
		System.out.println(getIndex(a, 61, 0, 6));

	}

	public static int getIndex(int[] a, int value) {
		if (a == null && !isSorted((int[]) a))
			return -1;
		int mid;
		int l = 0;
		int r = a.length - 1;
		while (l <= r) {
			mid = l + ((r - l) / 2);
			if (a[mid] == value) {
				return mid;
			} else {
				if (a[mid] < value) {
					l = mid + 1;
				} else {
					r = mid - 1;
				}
			}
		}
		return -1;

	}

	public static int getIndex(int[] a, int value, int l, int r) {
		if (a == null && !isSorted((int[]) a))
			return -1;
		if (l > r)
			return -1;
		int mid = l + ((r - l) / 2);
		if (a[mid] == value) {
			return mid;
		} else {
			if (a[mid] < value) {
				l = ++mid;
			} else {
				r = --mid;
			}
		}
		return getIndex(a, value, l, r);

	}

	/**
	 * isSorted(long[] a) return false if a == null
	 * 
	 * @param a
	 * @return true or false
	 */
	public static boolean isSorted(long[] a) {
		if (a == null)
			return false;
		boolean rt = true;
		for (int i = 1; i < a.length; i++) {
			if (a[i - 1] > a[i])
				return false;
		}
		return rt;
	}

	/**
	 * isSorted(long[] a) return false if a == null
	 * 
	 * @param a
	 * @return true or false
	 */
	public static boolean isSorted(int[] a) {
		if (a == null)
			return false;
		boolean rt = true;
		for (int i = 1; i < a.length; i++) {
			if (a[i - 1] > a[i])
				return false;
		}
		return rt;
	}
}
