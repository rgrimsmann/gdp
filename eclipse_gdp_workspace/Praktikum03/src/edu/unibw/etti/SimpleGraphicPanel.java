package edu.unibw.etti;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Panel;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.Toolkit;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Arc2D;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.tools.Tool;

/**
 * Mit der Klasse <tt>SimpleGraphicPanel</tt> k&ouml;nnen einfache
 * Graphikobjekte gemalt werden. Es k&ouml;nnen Dreiecke, Vierecke, Kreise und
 * Linien gemalt werden. Die Graphikobjekte k&ouml;nnen in verschiedenen Farben
 * gemalt werden. Dazu muss die Klasse <tt>Color</tt> importiert werden:
 * <tt>import java.awt.Color;</tt>.
 *
 * Sobald ein Graphikobjekt erfolgreich gemalt wurde (zu kleine Objekte werden
 * nicht gemalt) öffnet sich das Graphikfenster. Das Fenster hat einen weißen
 * Hintergrund. Die Breite ist 600 Pixel und die Höhe 400 Pixel. Der Nullpunkt
 * liegt links unten in der Ecke. Die x-Achse ist nach rechts und die y-Achse
 * ist nach oben orientiert. Die Einheiten sind Pixel.
 */
public class SimpleGraphicPanel {

    private static final ArrayList<Shape> SHAPES = new ArrayList<>();
    private static final ArrayList<Color> COLORS = new ArrayList<>();
    private static final ArrayList<Boolean> FILLS = new ArrayList<>();
    private static InternGraphicPanel internPanel = null;

    private SimpleGraphicPanel() {
        super();
    }

    /**
     * Mit dieser Klassenmethode kann ein Viereck gemalt werden. Dazu
     * m&uuml;ssen die x- und y-Werte der vier Ecken entweder im oder gegen den
     * Uhrzeigersinn angegeben werden. Au&szlig;erdem kann angegeben werden,
     * welche Farbe das Viereck haben soll und ob es gef&uuml;llt werden soll.
     * <p>
     * Z.B. erzeugt
     * <tt>SimpleGraphicPanel.drawQuadrilateral(Color.RED,true,0,0,50,0,50,50,0,50)</tt>
     * ein rotes, gef&uuml;lltes Quadrat mit Kantenl&auml;nge 50 in der linken
     * unteren Ecke des Fensters.
     *
     * @param color Die Farbe, in der das Viereck gemalt werden soll.
     * @param fill Gibt an, ob das Viereck mit Farbe gef&uuml;llt werden soll.
     * @param x1 x-Wert der 1. Ecke
     * @param y1 y-Wert der 1. Ecke
     * @param x2 x-Wert der 2. Ecke
     * @param y2 y-Wert der 2. Ecke
     * @param x3 x-Wert der 3. Ecke
     * @param y3 y-Wert der 3. Ecke
     * @param x4 x-Wert der 4. Ecke
     * @param y4 y-Wert der 4. Ecke
     * @return Gibt zur&uuml;ck, ob das Viereck gemalt wurde. Ein zu kleines
     * Viereck wird nicht gemalt.
     */
    static public boolean drawQuadrilateral(Color color, boolean fill,
            double x1, double y1, double x2, double y2,
            double x3, double y3, double x4, double y4) {

        if (color == null
                || Double.isNaN(x1) || Double.isNaN(y1)
                || Double.isNaN(x2) || Double.isNaN(y2)
                || Double.isNaN(x3) || Double.isNaN(y3)
                || Double.isNaN(x4) || Double.isNaN(y4)) {
            return false;
        }

        if (Math.hypot(x2 - x1, y2 - y1) < 1.0
                && Math.hypot(x3 - x2, y3 - y2) < 1.0
                && Math.hypot(x4 - x3, y4 - y3) < 1.0
                && Math.hypot(x1 - x4, y1 - y4) < 1.0) {
            return false;
        }

        Path2D path = new Path2D.Double();
        path.moveTo(x1, y1);
        path.lineTo(x2, y2);
        path.lineTo(x3, y3);
        path.lineTo(x4, y4);
        path.lineTo(x1, y1);
        SHAPES.add(path);
        COLORS.add(color);
        FILLS.add(fill);

        if (internPanel == null) {
            internPanel = new InternGraphicPanel();
            internPanel.show(Color.WHITE);
        }

        internPanel.repaint();
        return true;
    }

    /**
     * Mit dieser Klassenmethode kann ein Kreis gemalt werden. Dazu wird die
     * linke-untere Ecke (x,y) des umschlie&szlig;enden Quadrats und der
     * Durchmesser angegeben. Au&szlig;erdem kann angegeben werden, welche Farbe
     * der Kreis haben soll und ob er gef&uuml;llt werden soll.
     * <p>
     * Z.B. erzeugt
     * <tt>SimpleGraphicPanel.drawCircle(Color.ORANGE,false,50,50,50)</tt>
     * einen orangen, nicht gef&uuml;llten Kreis.
     *
     * @param color Die Farbe, in der der Kreis gemalt werden soll.
     * @param fill Gibt an, ob der Kreis mit Farbe gef&uuml;llt werden soll.
     * @param x x-Wert der linke-untere Ecke des umschlie&szlig;enden Quadrats
     * @param y y-Wert der linke-untere Ecke des umschlie&szlig;enden Quadrats
     * @param dim Durchmesser des Kreis
     * @return Gibt zur&uuml;ck, ob der Kreis gemalt wurde. Ein zu kleiner Kreis
     * wird nicht gemalt.
     */
    public static boolean drawCircle(Color color, boolean fill,
            double x, double y, double dim) {

        if (color == null
                || Double.isNaN(x) || Double.isNaN(y)
                || Double.isNaN(dim)) {
            return false;
        }

        if ((int) dim <= 0.5) {
            return false;
        }

        SHAPES.add(new Arc2D.Double(x, y, dim, dim, 0, 360,
                Arc2D.CHORD));
        COLORS.add(color);
        FILLS.add(fill);

        if (internPanel == null) {
            internPanel = new InternGraphicPanel();
            internPanel.show(Color.WHITE);
        }

        internPanel.repaint();
        return true;
    }

    /**
     * Mit dieser Klassenmethode kann ein Dreieck gemalt werden. Dazu
     * m&uuml;ssen die x- und y-Werte der drei Ecken angegeben werden.
     * Au&szlig;erdem kann angegeben werden, welche Farbe das Dreieck haben soll
     * und ob es gef&uuml;llt werden soll.
     * <p>
     * Z.B. erzeugt
     * <tt>SimpleGraphicPanel.drawTriangle(Color.GREEN,true,100,0,200,0,150,100)</tt>
     * ein grünes, gef&uuml;lltes Dreieck.
     *
     * @param color Die Farbe, in der das Dreieck gemalt werden soll.
     * @param fill Gibt an, ob das Dreieck mit Farbe gef&uuml;llt werden soll.
     * @param x1 x-Wert der 1. Ecke
     * @param y1 y-Wert der 1. Ecke
     * @param x2 x-Wert der 2. Ecke
     * @param y2 y-Wert der 2. Ecke
     * @param x3 x-Wert der 3. Ecke
     * @param y3 y-Wert der 3. Ecke
     * @return Gibt zur&uuml;ck, ob das Dreieck gemalt wurde. Ein zu kleines
     * Dreieck wird nicht gemalt.
     */
    public static boolean drawTriangle(Color color, boolean fill,
            double x1, double y1, double x2, double y2,
            double x3, double y3) {

        if (color == null
                || Double.isNaN(x1) || Double.isNaN(y1)
                || Double.isNaN(x2) || Double.isNaN(y2)
                || Double.isNaN(x3) || Double.isNaN(y3)) {
            return false;
        }

        if (Math.hypot(x2 - x1, y2 - y1) < 1.0
                && Math.hypot(x3 - x2, y3 - y2) < 1.0
                && Math.hypot(x1 - x3, y1 - y3) < 1.0) {
            return false;
        }

        Path2D path = new Path2D.Double();
        path.moveTo(x1, y1);
        path.lineTo(x2, y2);
        path.lineTo(x3, y3);
        path.lineTo(x1, y1);
        SHAPES.add(path);
        COLORS.add(color);
        FILLS.add(fill);

        if (internPanel == null) {
            internPanel = new InternGraphicPanel();
            internPanel.show(Color.WHITE);
        }

        internPanel.repaint();
        return true;
    }

    /**
     * Mit dieser Klassenmethode kann eine Linie gemalt werden. Dazu m&uuml;ssen
     * die x- und y-Werte der zwei Endpunkte angegeben werden.
     *
     * @param color Die Farbe, in der die Linie gemalt werden soll.
     * @param x1 x-Wert der 1. Punkt
     * @param y1 y-Wert der 1. Punkt
     * @param x2 x-Wert der 2. Punkt
     * @param y2 y-Wert der 2. Punkt
     * @return Gibt zur&uuml;ck, ob die Linie gemalt wurde. Eine zu kurze Linie
     * wird nicht gemalt.
     */
    public static boolean drawLine(Color color,
            double x1, double y1, double x2, double y2) {

        if (color == null
                || Double.isNaN(x1) || Double.isNaN(y1)
                || Double.isNaN(x2) || Double.isNaN(y2)) {
            return false;
        }

        if (Math.hypot(x2 - x1, y2 - y1) < 1.0) {
            return false;
        }

        Path2D path = new Path2D.Double();
        path.moveTo(x1, y1);
        path.lineTo(x2, y2);
        path.lineTo(x1, y1);
        SHAPES.add(path);
        COLORS.add(color);
        FILLS.add(true);

        if (internPanel == null) {
            internPanel = new InternGraphicPanel();
            internPanel.show(Color.WHITE);
        }

        internPanel.repaint();
        return true;
    }

    /**
     * Mit dieser Klassenmethode kann eine zuf&auml;llige Farbe erzeugt werden.
     *
     * @return Gibt eine zuf&auml;llige Farbe zur&uuml;ck.
     */
    public static Color getRandomColor() {
        return new Color((float) Math.random(),
                (float) Math.random(),
                (float) Math.random());
    }

    /**
     * Mit dieser Klassenmethode kann eine zuf&auml;llige helle Farbe erzeugt
     * werden.
     *
     * @return Gibt eine zuf&auml;llige helle Farbe zur&uuml;ck.
     */
    public static Color getRandomBrightColor() {
        return Color.getHSBColor((float) Math.random(), 1.0f, 1.0f);
    }

    private static class InternGraphicPanel extends Panel implements
            MouseWheelListener {

        private Frame frame = null;
        private int width = 0;
        private int height = 0;
        private double zoom = 1.0;
        private Color background = Color.white;

        public void show( Color background) {
        	Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            this.width = d.width;
            this.height = d.height;
            this.background = background;
            if (frame == null) {
                frame = new Frame();
                frame.setTitle("Graphic Panel");
                frame.addWindowListener(new InternWindowAdapter());
                frame.add(this);
                frame.pack();
                frame.setSize(width, height);
                frame.addMouseWheelListener(this);
                frame.setVisible(true);
            } else {
                if (frame.getSize().width != width || frame.getSize().height
                        != height) {
                    frame.setSize(width, height);
                }
            }
        }

        @Override
        public void paint(Graphics graphics) {

            Dimension vDimension = getSize();
            this.width = vDimension.width;
            this.height = vDimension.height;
            BufferedImage bufferedImage = (BufferedImage) createImage(
                    vDimension.width,
                    vDimension.height);

            Graphics2D graphics2d = bufferedImage.createGraphics();
            graphics2d.setBackground(background);
            graphics2d.clearRect(0, 0, vDimension.width,
                    vDimension.height);
            graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                    RenderingHints.VALUE_ANTIALIAS_ON);
            graphics2d.setRenderingHint(RenderingHints.KEY_RENDERING,
                    RenderingHints.VALUE_RENDER_QUALITY);

            graphics2d.setTransform(
                    new AffineTransform(zoom, 0, 0, -zoom, 0.0, height));
            for (int i = 0; i < SHAPES.size(); i++) {
                graphics2d.setColor(COLORS.get(i));
                graphics2d.draw(SHAPES.get(i));
                if (FILLS.get(i)) {
                    graphics2d.fill(SHAPES.get(i));
                }
            }
            graphics2d.dispose();

            graphics.drawImage(bufferedImage, 0, 0, vDimension.width,
                    vDimension.height,
                    this);
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {

            if (e.getWheelRotation() > 0) {
                zoom = zoom * 1.1;
            }
            if (e.getWheelRotation() < 0) {
                zoom = zoom * 0.9;
            }
            if (zoom < 0.001) {
                zoom = 0.001;
            }
            if (zoom > 5.0) {
                zoom = 5.0;
            }
            repaint();
        }
    }

    private static class InternWindowAdapter extends WindowAdapter {

        @Override
        public void windowClosing(WindowEvent e) {
            e.getWindow().dispose();                   
            System.exit(0);                             
        }
    }
}
