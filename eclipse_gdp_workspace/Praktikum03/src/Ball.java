
import java.awt.Color;
import java.awt.Graphics;

public class Ball {

    private double x;
    private double y;

    private double x_speed;
    private double y_speed;

    private double x_force;
    private double y_force;

    private double x_max;
    private double y_max;
    
    private int size = 4;

    public Ball(double x, double y, double x_speed, double y_speed, double x_force, double y_force, double x_max, double y_max) {

        this.x = x;
        this.y = y;

        this.x_speed = x_speed;
        this.y_speed = y_speed;

        this.x_force = x_force;
        this.y_force = y_force;

        this.x_max = x_max;
        this.y_max = y_max;

    }

    public void wurf() {

        if (y < y_max) {
            //Aenderung der Richtung wenn der Scheitelpunkt erreicht ist
            if (Math.abs(y_speed) < 0.6) {

                y_speed = 0.6;

            }

            //Nimmt immer mehr ab die Geschwindigkeit in Y
            if (y_speed < 0) {
                y = y + y_speed;
                y_speed = y_speed * y_force;
            } else {
                y = y + y_speed;
                y_speed = y_speed * (1 / y_force);
            }
            
            //Seitwertsbewegung
            x += x_speed;
                   
        }

    }

    public void paint(Graphics g) {
        g.setColor(Color.white);
        g.fillOval((int) x, (int) y, size, size);
        g.setColor(Color.black);
        g.drawOval((int) x, (int) y, size, size);
    }

    public void drawInfos() {
        System.out.println(
                "Y: " + y
                + "\tY_Speed: " + y_speed
                + "\tY_Force: " + y_force+
                
                "\tX: " + x
                + "\tX_Speed: " + x_speed
                + "\tX_Force: " + x_force                
        );
    }

}
