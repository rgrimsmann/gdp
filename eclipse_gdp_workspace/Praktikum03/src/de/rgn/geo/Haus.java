package de.rgn.geo;

import java.awt.Color;

import edu.unibw.etti.*;

public class Haus {

	public static void main(String[] args) {
		if (test()) {
			run();
		} else {
			System.out.println("Test Fehlgeschlagen");
		}
		// TODO Auto-generated method stub
	}

	private static void run() {
		boolean k = maleHaus(10.0, 10.0, 100.0, 10.0, .5);
		System.out.println(k);
		boolean zf = maleBuntesHaus(200d, 220d, 270d, 90d, .5, SimpleGraphicPanel.getRandomColor(),
				SimpleGraphicPanel.getRandomColor());
		System.out.println(zf);
	}

	/**
	 * Überprüft die Parameter auf Zulässigkeit falls, wenn eine von den
	 * Parametern kleiner 0 wird und wenn s größer 1 ist
	 * 
	 * @param ax
	 * @param ay
	 * @param bx
	 * @param by
	 * @param s
	 * @return boolean ergebnis
	 */
	private static boolean checkParam(double ax, double ay, double bx, double by, double s) {
		boolean ok = true;
		if (ax < 0.0 || ay < 0.0 || bx < 0.0 || by < 0.0 || (s < 0.0 && s > 1.0)) {
			ok = false;
		}
		return ok;
	}

	private static boolean maleHaus(double ax, double ay, double bx, double by, double s) {
		// TODO Auto-generated method stub
		return maleHaus(new Point(ax, ay), new Point(bx, by), s);
	}

	private static boolean maleHaus(Point a, Point b, double s) {
		boolean paramOk = checkParam(a.X(), a.Y(), b.X(), b.Y(), s);
		Point ha = new Point(a);
		Point hb = new Point(b);
		Point hc = Geometrie.getCPoint(ha, hb);
		Point hd = Geometrie.getDPoint(ha, hb);
		Point he = Geometrie.getEPoint(hc, hd, s);
		boolean dOk = false; // Dreieck
		boolean qOk = false; // Quadrat
		qOk = SimpleGraphicPanel.drawQuadrilateral(Color.black, true, ha.X(), ha.Y(), hb.X(), hb.Y(), hc.X(), hc.Y(),
				hd.X(), hd.Y());

		dOk = SimpleGraphicPanel.drawTriangle(Color.red, true, hc.X(), hc.Y(), hd.X(), hd.Y(), he.X(), he.Y());

		return !(paramOk && (dOk || qOk) && (s > 1d));
	}

	private static boolean maleBuntesHaus(double ax, double ay, double bx, double by, double s, Color haus,
			Color dach) {
		// TODO Auto-generated method stub
		boolean maleOk = checkParam(ax, ay, bx, by, s);
		return maleBuntesHaus(new Point(ax, ay), new Point(bx, by), haus, dach, s) && maleOk;
	}

	private static boolean maleBuntesHaus(Point a, Point b, Color hausC, Color dachC, double s) {
		boolean paramOk = checkParam(a.X(), a.Y(), b.X(), b.Y(), s);
		Point ha = new Point(a);
		Point hb = new Point(b);
		Point hc = Geometrie.getCPoint(ha, hb);
		Point hd = Geometrie.getDPoint(ha, hb);
		Point he = Geometrie.getEPoint(hc, hd, s);
		boolean dOk = false; // Dreieck
		boolean qOk = false; // Quadrat
		qOk = SimpleGraphicPanel.drawQuadrilateral(hausC, true, ha.X(), ha.Y(), hb.X(), hb.Y(), hc.X(), hc.Y(), hd.X(),
				hd.Y());

		dOk = SimpleGraphicPanel.drawTriangle(dachC, true, hc.X(), hc.Y(), hd.X(), hd.Y(), he.X(), he.Y());

		return paramOk && (dOk || qOk) && (s > 1d) ? false : true;
	}

	private static boolean maleHaus(double ax, double ay, double bx, double by) {
		return maleHaus(ax, ay, bx, by, Math.random());
	}

	private static boolean maleBuntesHaus(double ax, double ay, double bx, double by, Color haus, Color dach) {
		return maleBuntesHaus(ax, ay, bx, by, Math.random(), haus, dach);
	}

	private static boolean test() {
		// TODO Auto-generated method stub
		// Quadrat
		Point qa = new Point(100d, 100d);
		Point qb = new Point(150d, 70d);
		Point qc = Geometrie.getCPoint(qa, qb);
		Point qd = Geometrie.getDPoint(qa, qb);
		// Dreieck
		Point da = new Point(200d, 100d);
		Point db = new Point(270d, 110d);
		Point dc = Geometrie.getEPoint(db, da, .3);
		// Haus
		Point ha = new Point(300d, 100d);
		Point hb = new Point(350d, 50d);
		Point hc = Geometrie.getCPoint(ha, hb);
		Point hd = Geometrie.getDPoint(ha, hb);
		Point he = Geometrie.getEPoint(hc, hd, .8);

		SimpleGraphicPanel.drawQuadrilateral(Color.BLUE, true, qa.X(), qa.Y(), qb.X(), qb.Y(), qc.X(), qc.Y(), qd.X(),
				qd.Y());

		SimpleGraphicPanel.drawTriangle(Color.green, true, da.X(), da.Y(), db.X(), db.Y(), dc.X(), dc.Y());

		SimpleGraphicPanel.drawQuadrilateral(Color.black, true, ha.X(), ha.Y(), hb.X(), hb.Y(), hc.X(), hc.Y(), hd.X(),
				hd.Y());

		SimpleGraphicPanel.drawTriangle(Color.red, true, hc.X(), hc.Y(), hd.X(), hd.Y(), he.X(), he.Y());
		boolean test = true;
		test = (test && !maleHaus(ha.X(), ha.Y(), hb.X(), hb.Y(), 11d)); // False
		test = (test && maleHaus(0.0, 0.0, 0.0, 0.0, 0.0));
		test = (test && !maleHaus(0.0, -1.0, -2.0, 0.0, 0.0));
		test = (test && maleHaus(ha.X(), ha.Y(), hb.X(), hb.Y(), 1.0d)); //
		// true
		// erwartet
		test = (test && !maleBuntesHaus(ha.X(), ha.Y(), hb.X(), hb.Y(), 1.5d, Color.BLACK, Color.RED)); // false
		test = (test && maleBuntesHaus(ha.X(), ha.Y(), hb.X(), hb.Y(), 1.0d, Color.BLACK, Color.RED)); // true
		test = (test && maleHaus(ha, hb, 1.0d)); // true erwartet
		test = (test && !maleHaus(ha, hb, 1.1d)); // false erwartet
		test = (test && maleBuntesHaus(ha, hb, Color.black, Color.RED, 1.0d));
		test = (test && !maleBuntesHaus(ha, hb, Color.black, Color.RED, 1.1d));
		test = (test && maleHaus(ha.X(), ha.Y(), hb.X(), hb.Y()));
		test = (test && maleBuntesHaus(ha.X(), ha.Y(), hb.X(), hb.Y(), Color.BLACK, Color.red));
		return test;
	}

}
