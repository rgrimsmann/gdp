package de.rgn.geo;

/**
 * Point Class
 * 
 * @author Robin Grimsmann Stellt einen Punkt im kartesischen Koordinatensystem
 *         dar
 *
 */
public class Point {
	private double x, y;

	public Point() {
		setX(0.0);
		setY(0.0);
	}

	/**
	 * Initialisiert den Punkt mit den Werten eines anderen Punktes
	 * 
	 * @param p
	 */
	public Point(Point p) {
		setX(p.X());
		setY(p.Y());
	}

	/**
	 * Construktor setzt die Parameter des Punktes manuell
	 * 
	 * @param x
	 * @param y
	 */
	public Point(double x, double y) {
		setX(x);
		setY(y);
	}

	/**
	 * 
	 * @return double x
	 */
	public double X() {
		return x;
	}

	/**
	 * 
	 * @return double y
	 */
	public double Y() {
		return y;
	}

	/**
	 * Setzt den X Wert
	 * 
	 * @param x1
	 *            double
	 */
	public void setX(double x1) {
		x = x1;
	}

	/**
	 * Setzt den Y Wert
	 * 
	 * @param y1
	 */
	public void setY(double y1) {
		y = y1;
	}

	public boolean equals(Object o) {
		boolean ret = false;
		if (o instanceof Point) {
			ret = (((Point) o).X() == this.X()) && (((Point) o).Y() == this.Y());
		}
		return ret;
	}

	public String toString() {
		return ("(" + this.X() + "," + this.Y() + ")");
	}
}
