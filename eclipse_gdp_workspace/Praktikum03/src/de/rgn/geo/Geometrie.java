package de.rgn.geo;

public class Geometrie {

	public static double berechneCx(double ay, double bx, double by) {
		return bx + (ay - by);
	}

	public static double berechneCy(double ax, double bx, double by) {
		return by + (bx - ax);
	}

	public static double berechneDx(double ax, double ay, double by) {
		return ax + (ay - by);
	}

	public static double berechneDy(double ax, double ay, double bx) {
		return ay + (bx - ax);
	}

	public static double berechneEx(double dx, double dy, double cx, double cy, double s) {
		//s = (s <= 0d || s > 1d) ? 0.5d:s; //Verhindert das nur ein Strich als dach gemalt wird
		return (1.0d - s) * dx + s * cx + Math.sqrt(s * (1.0d - s)) * (dy - cy);
	}

	public static double berechneEy(double dx, double dy, double cx, double cy, double s) {
		//s = (s <= 0d || s > 1d) ? 0.5d:s;
		return (1.0d - s) * dy + s * cy + Math.sqrt(s * (1.0d - s)) * (cx - dx);
	}

	public static Point getCPoint(Point a, Point b) {
		Point p = new Point(berechneCx(a.Y(), b.X(), b.Y()), berechneCy(a.X(), b.X(), b.Y()));
		return p;
	}

	public static Point getDPoint(Point a, Point b) {
		Point p = new Point(berechneDx(a.X(), a.Y(), b.Y()), berechneDy(a.X(), a.Y(), b.X()));
		return p;
	}

	public static Point getEPoint(Point c, Point d, double s) {
		Point p = new Point(berechneEx(d.X(), d.Y(), c.X(), c.Y(), s), berechneEy(d.X(), d.Y(), c.X(), c.Y(), s));
		return p;
	}
}
