
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import edu.unibw.etti.graphics.GraphicsPanel;
import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

/**
 *
 * @author kang
 */
public class Test {

	public static void main(String[] args) {
		GraphicsPanel panel = new GraphicsPanel("Titel", 600, 400);

		Graphics g = panel.createGraphics();

		g.setColor(Color.red);

		Ball[] balls = new Ball[1000];
		while (true) {
			for (int i = 0; i < balls.length; i++) {
				balls[i] = new Ball(250, // X
						210, // Y
						randDouble(-2, 2), // XSpeed
						randDouble(-2, -15), // YSpeed
						0, // XForce
						randDouble(0.9, 0.97), // YForce
						0, // XMax
						380);// YMAx
			}

			g.setColor(Color.orange);
			g.fillOval(200, 300, 50, 50);
			g.fillOval(250, 300, 50, 50);
			g.fillRect(235, 200, 35, 120);
			g.fillOval(228, 190, 50, 50);
			g.setColor(Color.black);
			g.drawLine(250, 190, 250, 210);

			GraphicsPanel.sleep(100);

			for (int i = 0; i < balls.length; i++) {

				g.setColor(Color.white);
				g.fillRect(0, 0, 600, 400);

				g.setColor(Color.orange);
				g.fillOval(200, 300, 50, 50);
				g.fillOval(250, 300, 50, 50);
				g.fillRect(235, 200, 35, 120);
				g.fillOval(228, 190, 50, 50);
				g.setColor(Color.black);
				g.drawLine(250, 190, 250, 210);

				for (Ball ball : balls) {
					ball.wurf();
					ball.paint(g);
				}

				// b1.wurf();
				// b1.drawInfos();
				// g.setColor(Color.red);
				// b1.paint(g);
				panel.showGraphics();
				GraphicsPanel.sleep(50);
				for (Ball ball : balls) {
					ball = null;
				}
			}

			System.out.println(randDouble(0.7, 0.77));
			GraphicsPanel.sleep(500);
		}
	}

	public static double randDouble(double min, double max) {

		Random rand = new Random();

		double randomNum = (rand.nextDouble() * (max - min)) + min;

		return randomNum;
	}
}
