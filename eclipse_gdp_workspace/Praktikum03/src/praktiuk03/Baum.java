package praktiuk03;

import java.awt.Color;
import java.util.Random;

import de.rgn.geo.Geometrie;
import edu.unibw.etti.SimpleGraphicPanel;

public class Baum {
	public static void main(String[] args) {
		run();
	}

	public static void run() {
		double ax = 800d;
		double ay = 60d;
		double bx = ax + 100;
		double by = 60d;
		System.out.println(drawTree(555, ax, ay, bx, by));

	}

	public static int drawTree(int h, double ax, double ay, double bx, double by, double s) {
		int cnt = 0;
		if (h == -1 || Math.sqrt(Math.pow((bx - ax), 2) + Math.pow((by - ay), 2)) < 2) {
			return cnt; 
		} else {
			h--;
			cnt++;
			boolean leaves = new Random().nextBoolean();
			double cx = Geometrie.berechneCx(ay, bx, by);
			double cy = Geometrie.berechneCy(ax, bx, by);
			double dx = Geometrie.berechneDx(ax, ay, by);
			double dy = Geometrie.berechneDy(ax, ay, bx);
			double ex = Geometrie.berechneEx(dx, dy, cx, cy, s);
			double ey = Geometrie.berechneEy(dx, dy, cx, cy, s);
			SimpleGraphicPanel.drawQuadrilateral(leaves ? SimpleGraphicPanel.getRandomColor() : Color.BLACK, true, ax, ay, bx, by, cx, cy, dx, dy);
			SimpleGraphicPanel.drawTriangle(leaves ? Color.white : SimpleGraphicPanel.getRandomColor(), true, dx, dy, cx, cy, ex, ey);
			cnt += drawTree(h, ex, ey, cx, cy, s);
			cnt += drawTree(h, dx, dy, ex, ey, s);
			return cnt;
		}
	}
	 public static int drawTree(int h, double ax, double ay, double bx, double by) {
		 int cnt = 0;
		 if (h == -1 || Math.sqrt(Math.pow((bx - ax), 2) + Math.pow((by - ay), 2)) < 2) {
				return cnt; 
			} else {
				h--;
				cnt++;
				double s = randomNumber(0.2, 0.8);
				double cx = Geometrie.berechneCx(ay, bx, by);
				double cy = Geometrie.berechneCy(ax, bx, by);
				double dx = Geometrie.berechneDx(ax, ay, by);
				double dy = Geometrie.berechneDy(ax, ay, bx);
				double ex = Geometrie.berechneEx(dx, dy, cx, cy, s);
				double ey = Geometrie.berechneEy(dx, dy, cx, cy, s);
				SimpleGraphicPanel.drawQuadrilateral(Color.BLACK, true, ax, ay, bx, by, cx, cy, dx, dy);
				SimpleGraphicPanel.drawTriangle(Color.green, false, dx, dy, cx, cy, ex, ey);
				cnt += drawTree(h, ex, ey, cx, cy);
				cnt += drawTree(h, dx, dy, ex, ey);
				return cnt;
			}
	 }
	 
	 public static double randomNumber(double grenze1, double grenze2) {
		 double s = Math.random();
		 double d = s * grenze2 + grenze1;
		 return d;
	}
	 
	
}
