
public class Physics {

    private double x_speed;
    private double y_speed;

    private double x_force;
    private double y_force;

    public Physics(double x_speed, double y_speed, double x_force, double y_force) {

        this.x_speed = x_speed;
        this.y_speed = y_speed;

        this.x_force = x_force;
        this.y_force = y_force;

    }
}
