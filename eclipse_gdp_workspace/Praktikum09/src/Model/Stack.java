package Model;

public class Stack {
	private SLLObject head = null;

	public void push(Object element) {
		head = new SLLObject(element, head);
	}

	public Object pop() {
		Object tmp = head.first;
		head = head.rest;
		return tmp;
	}

	public Object peek() {
		return head.first;
	}

	public boolean isEmpty() {
		return (head == null);
	}

}
