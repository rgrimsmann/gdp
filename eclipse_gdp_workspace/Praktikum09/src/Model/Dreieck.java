package Model;

import java.awt.Color;
import java.awt.Graphics;

public class Dreieck {
	private double hoehe;
	private double grundflaeche;
	private Position p;
	private Color innen;
	private Color außen;

	public Dreieck(Color innen, Color außen, Position p, double g, double h) {
		setHoehe(h);
		setGrundflaeche(g);
		setColors(innen, außen);
		setPosition(p);
	}

	public Dreieck(Color innen, Color außen, double x, double y, double g, double h) {
		setHoehe(h);
		setGrundflaeche(g);
		setColors(innen, außen);
		this.p = new Position(x, y);
	}

	public double getHoehe() {
		return hoehe;
	}

	public void setHoehe(double hoehe) {
		this.hoehe = hoehe;
	}

	public double getGrundflaeche() {
		return grundflaeche;
	}

	public void setGrundflaeche(double grundflaeche) {
		this.grundflaeche = grundflaeche;
	}

	public Color getInnen() {
		return innen;
	}

	public void setInnen(Color innen) {
		this.innen = innen;
	}

	public Color getAußen() {
		return außen;
	}

	public void setAußen(Color außen) {
		this.außen = außen;
	}

	public Position getPosition() {
		return p;
	}

	public void setPosition(Position p) {
		this.p = p;
	}

	public void setColors(Color innen, Color außen) {
		this.innen = innen;
		this.außen = außen;
	}

	public void anzeigen(Graphics g) {
		int[] x = { (int) p.getX(), (int) (p.getX() + grundflaeche), (int) (p.getX() + (grundflaeche / 2)) };
		int[] y = {(int) p.getY(), (int) p.getY(), (int) (p.getY() - hoehe)};
		if(innen != null && außen != null) {
			g.setColor(außen);
			g.drawPolygon(x, y, x.length);
			g.setColor(innen);
			g.fillPolygon(x, y, x.length);
		} else if(innen != null && außen == null){
			g.setColor(innen);
			g.fillPolygon(x, y, x.length);
		} else if(innen == null && außen != null) {
			g.setColor(außen);
			g.drawPolygon(x, y, x.length);
		} else {
			System.out.println("Zeichnen nicht möglich");
		}
	}
}
