package Model;

import java.awt.Color;

public class Ballon extends Kreis {
	private double schrittweite;

	public Ballon(double x, double y, Color innen, double sw) {
		super(x, y, 1, innen);
		schrittweite = sw;
		// TODO Auto-generated constructor stub
	}

	@Override
	public void move() {
		double r = getRadius();
		if (((getPosition().getX() + r + schrittweite) <= Position.X_MAX
				&& (getPosition().getX() - r - schrittweite) > 0
				&& ((getPosition().getY() + r + schrittweite) <= Position.Y_MAX
						&& (getPosition().getY() - r - schrittweite) > 0))) {
			setRadius(r + schrittweite);
			if (getRadius() <= 0) {
				schrittweite = schrittweite * (-1);
			}
		} else {
			schrittweite = schrittweite * (-1);
		}
	}
}
