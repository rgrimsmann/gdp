
public class KomplexeZahlenOperatoren {
	/**
	 * gibt die werte Der Komplexenzahl in der Polarform und der kartesischen
	 * Form
	 * 
	 * @param c
	 *            Komplexezahl
	 */
	public static void print(Komplexezahl c, boolean bogenmass) {
		String as = "";
		if (c != null && c instanceof Komplexezahl) {
			as += ((c.abs() > 1) ? c.abs()
					: (c.abs() < 1 && c.abs() > -1) ? c.abs()
							: (c.abs() == -1) ? "-" : c.abs())
					+ "exp(" + (c.getPhi() < 0 ? "-j" : "j") + (bogenmass ? Math.abs(c.getPhi()): c.calcPhi(false))
					+ ((bogenmass) ? ") Bogenmass" : ") Gradmass");
			System.out.println(as);
		} else {
			System.out.println("keine Komplexezahl!");
		}
	}

	/**
	 * Addition zweier Komplexerzahlen
	 * 
	 * @param a
	 *            Komplexezahl
	 * @param b
	 *            Komplexezahl
	 * @return new Komplexezahl(RE{a+b}, IM{a + b})
	 */
	public static Komplexezahl add(Komplexezahl a, Komplexezahl b) {
		if (a == null || b == null || !(a instanceof Komplexezahl) || !(b instanceof Komplexezahl))
			return null;
		else {
			return new Komplexezahl(a.getRealteil() + b.getRealteil(), a.getImanginärteil() + b.getImanginärteil());
		}
	}

	/**
	 * Subtraktion zweier Komplexerzahlen
	 * 
	 * @param a
	 *            Komplexezahl
	 * @param b
	 *            Komplexezahl
	 * @return new Komplexezahl(RE{a+b}, IM{a + b})
	 */
	public static Komplexezahl sub(Komplexezahl a, Komplexezahl b) {
		if (a == null || b == null || !(a instanceof Komplexezahl) || !(b instanceof Komplexezahl))
			return null;
		else {
			return new Komplexezahl(a.getRealteil() - b.getRealteil(), a.getImanginärteil() - b.getImanginärteil());
		}
	}

	/**
	 * Multipliziert zwei Komplexezahlen
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static Komplexezahl mult(Komplexezahl a, Komplexezahl b) {
		if (a == null || b == null || !(a instanceof Komplexezahl) || !(b instanceof Komplexezahl))
			return null;
		else {
			// bogenmass = false -> gradmass
			double winkel = a.getPhi() + b.getPhi();
			return new Komplexezahl(a.abs() * b.abs(), winkel, true);
		}
	}

	public static Komplexezahl mult2(Komplexezahl a, Komplexezahl b) {
		if (a == null || b == null || !(a instanceof Komplexezahl) || !(b instanceof Komplexezahl))
			return null;
		return new Komplexezahl(a.getRealteil() * b.getRealteil() - a.getImanginärteil() * b.getImanginärteil(),
				a.getRealteil() * b.getImanginärteil() + a.getImanginärteil() * b.getRealteil());
	}

	/**
	 * Dividiert zwei Komplexezahlen miteinander, erweitert komplex konjugiert
	 * um einen rein reelen nenner zu erhalten
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static Komplexezahl div(Komplexezahl a, Komplexezahl b) {
		if (a == null || b == null || !(a instanceof Komplexezahl) || !(b instanceof Komplexezahl))
			return null;
		double teiler = Math.pow(b.getRealteil(), 2.0) + Math.pow(b.getImanginärteil(), 2.0);
		double realteil = (a.getRealteil() * b.getRealteil() + a.getImanginärteil() * b.getImanginärteil()) / teiler;
		double imaginärteil = (a.getImanginärteil() * b.getRealteil() - a.getRealteil() * b.getImanginärteil())
				/ teiler;
		return new Komplexezahl(realteil, imaginärteil);

	}

	/**
	 * Der Betrag einer Komplexenzahl nach dem Satz des Pyhtagoras
	 * 
	 * @param a
	 * @return
	 */
	public static double abs(Komplexezahl a) {
		if (!(a instanceof Komplexezahl) || a == null)
			return Double.NaN;
		return Math.hypot(a.getRealteil(), a.getImanginärteil());
	}

	/**
	 * Errechnet die Zahl Phi aus dem Realteil und dem Imaginärteil
	 * 
	 * @see Mathematik 1 Kapitel Komplexezahlen Prof. Dr. Dr. T. Sturm
	 * @param c
	 * @return
	 */
	public static double phi(Komplexezahl c) { // Sinnfrei da die Klasse sich
												// schon alle daten genereiert
		if (!(c instanceof Komplexezahl) || c == null)
			return Double.NaN;
		return (c.getImanginärteil() >= 0) ? Math.acos(c.getRealteil() / abs(c))
				: (-1) * Math.acos(c.getRealteil() / abs(c));
	}

	public static double phi1(Komplexezahl c) {
		if (!(c instanceof Komplexezahl) || c == null) {
			return Double.NaN;
		}
		if (c.getRealteil() > 0) {
			return Math.atan(c.getImanginärteil() / c.getRealteil());
		} else if (c.getRealteil() < 0 && c.getImanginärteil() >= 0) {
			return Math.atan2((c.getImanginärteil() / c.getRealteil()), Math.PI);
		} else if (c.getRealteil() < 0 && c.getImanginärteil() < 0) {
			return Math.atan2((c.getImanginärteil() / c.getRealteil()), (-1) * Math.PI);
		} else if (c.getRealteil() == 0 && c.getImanginärteil() > 0) {
			return Math.PI / 2;
		} else if (c.getRealteil() == 0 && c.getImanginärteil() < 0) {
			return (-1) * Math.PI / 2;
		} else {
			return Double.POSITIVE_INFINITY;
		}
	}
	
	public static double phi2(Komplexezahl c) {
		return Math.atan2(c.getImanginärteil(), c.getRealteil());
	}
}
