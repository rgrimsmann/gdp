/**
 * Komplexezahlen FOS IT 2014 Veränderung für das Studium TIKT JG 15
 * 
 * @author Robin Grimsmsann
 *
 */
public class Komplexezahl {
	private double re = 0;
	private double im = 0;
	private double cbetrag;
	private double cphi;
	boolean bogenmass = false; // Bogenmass oder Gradmass false = gradmass

	/**
	 * Initialierung mit einer Komplexenzahl mit default Werten
	 */
	public Komplexezahl() {
		re = 0;
		im = 0;
		cphi = 0;
		cbetrag = 0;
	}

	/**
	 * Initialierung mit den genannten Werten Erstellt intern die Darstellung
	 * nach Betrag und Winkel (Gradmass)
	 * 
	 * @param real
	 * @param img
	 */
	public Komplexezahl(double real, double img) {
		re = real;
		im = img;
		bogenmass = false;
		create(false, false);
	}

	/**
	 * Initialierung mit den genannten Werten Erstellt intern die kartesische
	 * Darstellung
	 * 
	 * @param betrag
	 * @param phi
	 * @param isBogenmass
	 */
	public Komplexezahl(double betrag, double phi, boolean isBogenmass) {
		cphi = phi;
		cbetrag = betrag;
		bogenmass = isBogenmass;
		create(true, false);
	}

	/**
	 * Konstruktor Kopiert die Daten aus der Komplexenzahl C in die neue Zahl
	 * Erstellt die exponential Darstellung mit einem Gradmass Winkel
	 * 
	 * @param c
	 */
	public Komplexezahl(Object c) {
		if (c instanceof Komplexezahl) {
			re = (((Komplexezahl) c).getRe());
			im = (((Komplexezahl) c).getIm());
			create(false, false); // erstellt die exp darstellung mit einem
									// Gradmass
		}
	}

	/**
	 * Interne Funktion die für die erstellung der e Funktion bzw der
	 * kartesischen Darstellung verantwortlich ist
	 * 
	 * @param euler
	 * @param bm
	 */
	private void create(boolean euler, boolean bm) {
		if (!euler) {
			cbetrag = Math.hypot(re, im);
			if (bm)
				cphi = Math.toRadians((im >= 0) ? Math.acos(re / cbetrag) : (-1) * Math.acos(re / cbetrag));
			else
				cphi = (im >= 0) ? Math.acos(re / cbetrag) : (-1) * Math.acos(re / cbetrag);
			bogenmass = bm;
		} else {
			re = cbetrag * Math.cos(((bogenmass) ? Math.toDegrees(cphi) : cphi));
			im = cbetrag * Math.sin(((bogenmass) ? Math.toDegrees(cphi) : cphi));
		}

	}

	/* Beginn der Getter und Setter */
	public double getIm() {
		return im;
	}

	public double getRe() {
		return re;
	}

	public double getBetrag() {
		return cbetrag;
	}

	public double getPhi() {
		return cphi;
	}

	public boolean isBogenmass() {
		return bogenmass;
	}

	/* Ende Getter und Setter */
	/**
	 * Rechnet Phi ins Bogenmass um, wenn es nicht schon im Bogenmass vorliegt
	 * 
	 * @param bm
	 * @return Phi im gewünschten Mass
	 */
	public double calcPhi(boolean bm) {
		if (bm && this.bogenmass) {
			return this.cphi;
		} else if (bm && !this.bogenmass) {
			return Math.toRadians(cphi);
		} else if (!bm && this.bogenmass) {
			return Math.toDegrees(cphi);
		} else {
			return Math.toDegrees(cphi);
		}
	}

	/**
	 * Erstellt einen String mit den Daten der Komplexenzahl, je wie es
	 * gebraucht wird in der exp Darstellung oder in der kartesischen
	 * Darstellung
	 * 
	 * @param euler
	 * @return
	 */
	public String toString(boolean euler) {
		String s = "";
		if (euler) {
			s = cbetrag + "exp(" + ((cphi < 0) ? "-" : "") + "j" + Math.abs(cphi) + ") "
					+ ((bogenmass) ? "Bogenmass" : "Gradmass");
		} else {
			s = re + " " + ((im > 0) ? "+" : "-") + " j" + Math.abs(im);
		}
		return s;
	}
	
	/**
	 * Vergleicht zwei Komplexezahlen miteinander
	 * @param o
	 * @return
	 */
	public boolean equals(Komplexezahl o) {
		if (o != null && o instanceof Komplexezahl) {
			if ((this.getRe() == o.getRe() && this.getIm() == o.getIm())
					|| ((this.getBetrag() == o.getBetrag()) && (this.getPhi() == o.getPhi()))) {
				return true;
			}
		}
		return false;
	}
}
