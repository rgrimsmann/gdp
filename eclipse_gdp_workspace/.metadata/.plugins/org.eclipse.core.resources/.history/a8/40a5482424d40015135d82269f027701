
public class KomplexeZahlenOperatoren {
	/**
	 * gibt die werte Der Komplexenzahl in der Polarform und der kartesischen
	 * Form
	 * 
	 * @param c
	 *            Komplexezahl
	 */
	public static void print(Komplexezahl c, boolean bogenmass) {
		String as = "";
		if (c != null && c instanceof Komplexezahl) {
			as += ((c.getBetrag() > 1) ? c.getBetrag()
					: (c.getBetrag() < 1 && c.getBetrag() > -1) ? c.getBetrag() : (c.getBetrag() == -1) ? "-" : "")
					+ "exp(" + (c.calcPhi(bogenmass) < 0 ? "-j" : "j") + Math.abs(c.calcPhi(bogenmass))
					+ ((bogenmass) ? ") Bogenmass" : ") Gradmass");
			System.out.println(as);
		} else {
			System.out.println("keine Komplexezahl!");
		}
	}

	/**
	 * Addition zweier Komplexerzahlen
	 * 
	 * @param a
	 *            Komplexezahl
	 * @param b
	 *            Komplexezahl
	 * @return new Komplexezahl(RE{a+b}, IM{a + b})
	 */
	public static Komplexezahl add(Komplexezahl a, Komplexezahl b) {
		if (a == null || b == null || !(a instanceof Komplexezahl) || !(b instanceof Komplexezahl))
			return null;
		else {
			return new Komplexezahl(a.getRe() + b.getRe(), a.getIm() + b.getIm());
		}
	}

	/**
	 * Subtraktion zweier Komplexerzahlen
	 * 
	 * @param a
	 *            Komplexezahl
	 * @param b
	 *            Komplexezahl
	 * @return new Komplexezahl(RE{a+b}, IM{a + b})
	 */
	public static Komplexezahl sub(Komplexezahl a, Komplexezahl b) {
		if (a == null || b == null || !(a instanceof Komplexezahl) || !(b instanceof Komplexezahl))
			return null;
		else {
			return new Komplexezahl(a.getRe() - b.getRe(), a.getIm() - b.getIm());
		}
	}

	/**
	 * Multipliziert zwei Komplexezahlen
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static Komplexezahl mult(Komplexezahl a, Komplexezahl b) {
		if (a == null || b == null || !(a instanceof Komplexezahl) || !(b instanceof Komplexezahl))
			return null;
		else {
			double winkel = 0;
			// bogenmass = false -> gradmass
			if (!a.isBogenmass() && b.isBogenmass()) {
				winkel = a.getPhi() + Math.toDegrees(b.getPhi());
			} else if (a.isBogenmass() && !b.isBogenmass()) {
				winkel = Math.toDegrees(a.getPhi()) + b.getPhi();
			} else {
				winkel = a.getPhi() + b.getPhi();
			}
			return new Komplexezahl(a.getBetrag() * b.getBetrag(), winkel, (a.isBogenmass() && b.isBogenmass()));
		}
	}

	public static Komplexezahl mult2(Komplexezahl a, Komplexezahl b) {
		if (a == null || b == null || !(a instanceof Komplexezahl) || !(b instanceof Komplexezahl))
			return null;
		return new Komplexezahl(a.getRe() * b.getRe() - a.getIm() * b.getIm(),
				a.getRe() * b.getIm() + a.getIm() * b.getRe());
	}

	/**
	 * Dividiert zwei Komplexezahlen miteinander, erweitert komplex konjugiert
	 * um einen rein reelen nenner zu erhalten
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static Komplexezahl div(Komplexezahl a, Komplexezahl b) {
		if (a == null || b == null || !(a instanceof Komplexezahl) || !(b instanceof Komplexezahl))
			return null;
		double teiler = Math.pow(b.getRe(), 2.0) + Math.pow(b.getIm(), 2.0);
		double realteil = (a.getRe() * b.getRe() + a.getIm() * b.getIm()) / teiler;
		double imaginärteil = (a.getIm() * b.getRe() - a.getRe() * b.getIm()) / teiler;
		return new Komplexezahl(realteil, imaginärteil);

	}
	
	/**
	 * Der Betrag einer Komplexenzahl nach dem Satz des Pyhtagoras
	 * @param a
	 * @return
	 */
	public static double abs(Komplexezahl a) {
		if (!(a instanceof Komplexezahl) && a == null)
			return Double.NaN;
		return Math.hypot(a.getRe(), a.getIm());
	}
	
	/**
	 * Errechnet die Zahl Phi aus dem Realteil und dem Imaginärteil
	 * 
	 * @see Mathematik 1 Kapitel Komplexezahlen Prof. Dr. Dr. T. Sturm 
	 * @param c
	 * @return
	 */
	public static double phi(Komplexezahl c) { // Sinnfrei da die Klasse sich
												// schon alle daten genereiert
		if (!(c instanceof Komplexezahl) && c == null)
			return Double.NaN;
		return (c.getIm() >= 0) ? Math.acos(c.getRe() / abs(c)) : (-1) * Math.acos(c.getRe() / abs(c));
	}
}
