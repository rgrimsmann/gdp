package PrimitiveDatentypen;

public class IntExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		byte a = 10; 
		byte b = (byte) (a + 118); // -128
		short s = 10;  
		int meineZahl = 123456789; 
		long l = 12345; 
		int c = 10; 
		int d = -c;              // d = -10 
		c = 10; 
		d = c*(10+20)-c;         // d = 290 
		c = 7/3;                 // c = 2; 
		d = 8/3;                 // d = 2; 
		c = 7%3;                 // c = 1; 
		d = 8%3;                 // d = 2; 
		c = 7; 
		boolean e = (c != 3);    // e = true; 
		c = 7; 
		e = (c < 3);             // e = false;
		
	}

}
