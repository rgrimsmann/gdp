package PrimitiveDatentypen;

public class StringExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String a = "Robin Grimsmann"; 
		String b = "Robin" + " " + "Grimsmann"; 
		String c = "Robin".concat(" ").concat("Grimsmann"); 
		int d = a.length();                       // d
			
		String e = ""; 
		String f = a.substring(0,5);              
		// f = "Robin" 
		String g = "\uD834\uDD1E";               // g = €
		char h = a.charAt(3);                     // h = 'r'  
		boolean i = (a.charAt(3) == b.charAt(2)); // i = false
	}

}
