package PrimitiveDatentypen;

public class BoolExample {
	
	public static void main(String[] args) {
		// Zulaessige Werte: true, false 
		boolean a = true; 
		boolean b = false; 
		// Operationen: 
		// Gleichheit == 
		boolean c = (a == b);                      // c = false 
		// Ungleichheit != 
		boolean d = (a != b);                      // d = true 
		// Negation ! 
		boolean e = !a;                            // e = false 
		// Konjunktion &&, & 
		boolean f = (a && b);                      // f = false 
		boolean g = (a & b);                       // g = false 
		f = ((a && b) && (b && a));                // f = false  
		// Disjunktion ||, | 
		boolean h = (a || b);                      // h = true 
		boolean i = (a | b);                       // i = true 
		h = ((a || b) || (b || a));                // h = true 
	}

}
