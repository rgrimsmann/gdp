package PrimitiveDatentypen;

public class DoubleFloatExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		float f = 123;  
		f = (float)123.4; // Compiler Fehler 
		f = 123.4f;  
		double d = 123;  
		d = 123.4;  
		d = 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1 + 0.1; 
		System.out.println(d); // Standardausgabe: 0.9999999999999999 
		d = 9/2;  
		System.out.println(d); // Standardausgabe: 4.0 
		d = 9.0/2;  
		System.out.println(d); // Standardausgabe: 4.5 
		d = 11.7; 
		byte b = (byte)d; 
		System.out.println(b); // Standardausgabe: 11 
		d = 128; 
		b = (byte)d; 
		System.out.println(b); // Standardausgabe: -128
				double a = 10; 
		double k = -a;           // k = -10.0 
		float c = 10; 
		double l = c*(10+20)-c;  // l = 290.0 
		double e = 7.0/3.0;      // e = 2.3333333333333335; 
		float m = 7.0f/3.0f;     // m = 2.3333333; 
		float g = 0.1f; 
		boolean h = (g == 0.1);  // h = false; 
		float i = 0.1f; 
		boolean j = (i > 0.1);   // c = true;

	}

}
